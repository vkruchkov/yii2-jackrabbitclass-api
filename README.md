Access to jackrabbitclass.com
=============================
Access to jackrabbitclass.com

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist vkruchkov/yii2-jackrabbitclass-api "*"
```

or add

```
"vkruchkov/yii2-jackrabbitclass-api": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \vkruchkov\jackrabbitclass\AutoloadExample::widget(); ?>```